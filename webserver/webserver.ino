#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>

#ifdef ESP32
#pragma message(THIS EXAMPLE IS FOR ESP8266 ONLY !)
#error Select ESP8266 board.
#endif

ESP8266WebServer server(80); // 80 is the port number

const char *ssid = "Decentrala";
const char *password = "";

String resSwitch1On = "Switch1 is On", resSwitch1Off = "Switch1 is Off",
       resSwitch2On = "Switch2 is On", resSwitch2Off = "Switch2 is Off";

void switch1On() {
  digitalWrite(4, HIGH);
  server.send(200, "text/html", resSwitch1On);
}

void switch1Off() {
  digitalWrite(4, LOW);
  server.send(200, "text/html", resSwitch1Off);
}

void switch2On() {
  digitalWrite(14, HIGH);
  server.send(200, "text/html", resSwitch2On);
}

void switch2Off() {
  digitalWrite(14, LOW);
  server.send(200, "text/html", resSwitch2Off);
}

void setup() {

  Serial.begin(74880);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
    delay(500);

  Serial.print(WiFi.localIP());

  server.on("/switch1on", switch1On);
  server.on("/switch1off", switch1Off);
  server.on("/switch2on", switch2On);
  server.on("/switch2off", switch2Off);

  server.enableCORS(true);
  server.begin();

  pinMode(14, OUTPUT); // D5
  pinMode(4, OUTPUT);  // D2

  digitalWrite(14, LOW);
  digitalWrite(4, LOW);
}

void loop() {
  server.handleClient();
  delay(1);
}